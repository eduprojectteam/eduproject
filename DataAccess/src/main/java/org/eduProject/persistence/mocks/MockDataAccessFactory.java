package org.eduProject.persistence.mocks;

import org.eduProject.framework.entities.Rule;
import org.eduProject.framework.persistence.IDataAccess;
import org.eduProject.framework.persistence.IDataAccessFactory;
import org.eduProject.persistence.config.EntityNames;
import org.eduProject.persistence.entities.InterpretationRule;
import org.eduProject.persistence.entities.MappingRule;
import org.eduProject.persistence.entities.MarketRule;

@MockDA
public class MockDataAccessFactory implements IDataAccessFactory {

	/**
	 * Creates an instance of a IDataAccess<T,K> <T>: the type of domain class
	 * <K>: the id type of the domain class
	 * 
	 * @param rule
	 *            The name of the domain class
	 * @return A new instance of IDataAccess<T,K>
	 */
	@Override
	public <T extends Rule> IDataAccess<T> get(String rule) {
		if (rule.equals(EntityNames.MarketRule)) {
			MockDataAccess<MarketRule> marRule = new MockDataAccess<MarketRule>(
					MarketRule.class);
			return (MockDataAccess<T>) marRule;
		}
		if (rule.equals(EntityNames.MappingRule)) {
			MockDataAccess<MappingRule> mapRule = new MockDataAccess<MappingRule>(
					MappingRule.class);
			return (MockDataAccess<T>) mapRule;
		}
		if (rule.equals(EntityNames.InterpretationRule)) {
			MockDataAccess<InterpretationRule> mapRule = new MockDataAccess<InterpretationRule>(
					InterpretationRule.class);
			return (MockDataAccess<T>) mapRule;
		}

		return null;
	}
}
