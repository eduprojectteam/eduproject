package org.eduProject.persistence.mocks;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.eduProject.persistence.entities.DefaultRule;
import org.eduProject.persistence.entities.InterpretationRule;
import org.eduProject.persistence.entities.MappingRule;
import org.eduProject.persistence.entities.MarketRule;
import org.eduProject.persistence.entities.StockCategory;
import org.eduProject.persistence.entities.VehicleAttribute;

public class DataStore {

	private static List<MarketRule> defaultMarketRules;
	private static List<MappingRule> defaultMappingRules;
	private static List<InterpretationRule> defaultInterpretationRules;

	static {
		defaultMarketRules = new ArrayList<MarketRule>();
		defaultMappingRules = new ArrayList<MappingRule>();
		defaultInterpretationRules = new ArrayList<InterpretationRule>();

		MarketRule r = new MarketRule();
		r.setActive(true);
		r.setBranch(1);
		r.setCountryNumber("uk");
		r.setRule("market-uk-new");
		r.setStockCategory(StockCategory.New);
		defaultMarketRules.add(r);

		r = new MarketRule();
		r.setActive(true);
		r.setBranch(1);
		r.setCountryNumber("uk");
		r.setRule("market-uk-used");
		r.setStockCategory(StockCategory.Used);
		defaultMarketRules.add(r);

		r = new MarketRule();
		r.setActive(false);
		r.setBranch(2);
		r.setCountryNumber("es");
		r.setRule("market-es-used");
		r.setStockCategory(StockCategory.Used);
		defaultMarketRules.add(r);

		MappingRule m = new MappingRule();
		m.setId(1L);
		m.setSourceValue("source");
		m.setTargetValue("target");
		m.setVehicleAttribute("attr");
		defaultMappingRules.add(m);

		m = new MappingRule();
		m.setId(2L);
		m.setSourceValue("source2");
		m.setTargetValue("target2");
		m.setVehicleAttribute("attr2");
		defaultMappingRules.add(m);

		m = new MappingRule();
		m.setId(3L);
		m.setSourceValue("source3");
		m.setTargetValue("target3");
		m.setVehicleAttribute("attr3");
		defaultMappingRules.add(m);

		InterpretationRule i = new InterpretationRule();
		i.setId(1L);
		i.setRules(Arrays.asList(new DefaultRule() {
			{
				setId(1L);
				setVehicleAttribute("engine");
				setVehicleAttributeValues(Arrays.asList("Rotax", "Rotarex",
						"Rex"));
			}
		}, new DefaultRule() {
			{
				setId(2L);
				setVehicleAttribute("frontMirror");
				setVehicleAttributeValues(Arrays.asList("low", "hybrid"));
			}
		}));
		i.setTargetVehicle(Arrays.asList(new VehicleAttribute() {
			{
				setVehicleAttribute("engine");
				setVehicleAttributeValue("Volvo");
			}
		}, new VehicleAttribute() {
			{
				setVehicleAttribute("frontMirror");
				setVehicleAttributeValue("chunked");
			}
		}, new VehicleAttribute() {
			{
				setVehicleAttribute("clutch");
				setVehicleAttributeValue("auto");
			}
		}));
		defaultInterpretationRules.add(i);

		i = new InterpretationRule();
		i.setId(2L);
		i.setRules(Arrays.asList(new DefaultRule() {
			{
				setId(3L);
				setVehicleAttribute("engine2");
				setVehicleAttributeValues(Arrays.asList("Rotax2", "Rotarex2",
						"Rex2"));
			}
		}, new DefaultRule() {
			{
				setId(4L);
				setVehicleAttribute("frontMirror2");
				setVehicleAttributeValues(Arrays.asList("low2", "hybrid2"));
			}
		}));
		i.setTargetVehicle(Arrays.asList(new VehicleAttribute() {
			{
				setVehicleAttribute("engine2");
				setVehicleAttributeValue("Volvo2");
			}
		}, new VehicleAttribute() {
			{
				setVehicleAttribute("frontMirror2");
				setVehicleAttributeValue("chunked2");
			}
		}, new VehicleAttribute() {
			{
				setVehicleAttribute("clutch2");
				setVehicleAttributeValue("auto2");
			}
		}));
		defaultInterpretationRules.add(i);
	}

	private static HashMap<Type, List> rules = new HashMap<Type, List>() {
		{
			put(MarketRule.class, new ArrayList<MarketRule>(defaultMarketRules));
			put(MappingRule.class, new ArrayList<MappingRule>(
					defaultMappingRules));
			put(InterpretationRule.class, new ArrayList<InterpretationRule>(
					defaultInterpretationRules));
		}
	};

	public static <T> List<T> get(Class<T> type) {

		List<T> list = rules.get(type);
		return list;
	}

	public static <T> void set(List<T> list, Class<T> type) {

		rules.put(type, list);
	}
}
