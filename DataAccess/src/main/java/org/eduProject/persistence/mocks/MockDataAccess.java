package org.eduProject.persistence.mocks;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.eduProject.framework.TypeException;
import org.eduProject.framework.entities.Rule;
import org.eduProject.framework.entities.RuleKey;
import org.eduProject.framework.persistence.IDataAccess;

/**
 * Defines mock implementation for CRUD persistence functionality
 * 
 * @author paulm
 *
 * @param <T>
 *            Defines a Rule object
 */
// public class MockDataAccess<T extends Rule> implements IDataAccess<T> {

public class MockDataAccess<T extends Rule> implements IDataAccess<T> {

	private final Class<T> type;

	public MockDataAccess(Class<T> type) {

		this.type = type;
		System.out.println(type);
	}

	@Override
	public boolean post(T rule) {
		List<T> rules = DataStore.<T> get(type);

		if (rule.getClass().getName().equals(type.getName())) {
			rules.add(rule);
			DataStore.set(rules, type);
			return true;
		}

		return false;

	}

	@Override
	public boolean put(RuleKey id, T object) {

		List<T> rules = DataStore.<T> get(type);
		T found = find(rules, id);

		if (!(object.getClass().getName().equals(type.getName()))
				|| found == null) {
			return false;

		}

		rules.remove(found);
		rules.add(object);
		DataStore.set(rules, type);

		return true;

	}

	@Override
	public boolean delete(RuleKey id) {

		List<T> rules = DataStore.<T> get(type);
		T found = find(rules, id);
		if (found == null) {
			return false;
		}

		rules.remove(found);
		DataStore.set(rules, type);

		return true;
	}

	/**
	 * Gets the <T> defined by the given id
	 */
	@Override
	public T get(RuleKey id) {

		List<T> rules = DataStore.<T> get(type);
		T found = find(rules, id);
		
		return found;
	}

	/**
	 * Converts the given object to the <T extends Rule> type
	 * 
	 * @param type
	 *            The type to convert to
	 * @return The type object as <T extends Rule>
	 * @throws ClassCastException
	 *             Exception is thrown when the given object is not of a type <T
	 *             extends Rule>
	 */
	@Override
	public T getAsRule(Object type) throws TypeException {

		T t = null;
		try {

			t = (T) type;
		} catch (ClassCastException e) {

			throw new TypeException();
		}

		return t;
	}

	/**
	 * Finds a T with the given id
	 * 
	 * @param id
	 * @return
	 */
	private T find(List<T> rules, RuleKey key) {

		// go through all objects in the datastore
		for (T t : rules) {

			Map<String, String> ruleFields = new HashMap<String, String>();
			boolean isMatch = true;

			// get all fields of the current element from the datastore - RULE
			// properties
			for (Field f : t.getClass().getDeclaredFields()) {

				// try to put all properties of the element in a map
				try {
					String s = f.getName();

					f.setAccessible(true);
					Object val = f.get(t);
					ruleFields.put(s, val.toString());
					f.setAccessible(false);

				} catch (IllegalArgumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			// go through all elements in the KEY properties
			for (Entry<String, String> keyEntry : key.entrySet()) {

				String keyEntryKey = keyEntry.getKey();
				String keyEntryValue = keyEntry.getValue();

				// find a RULE field which matches the current KEY field
				if (!ruleFields.containsKey(keyEntryKey)) {
					isMatch = false;
					break;
				}

				// check if the RULE field has the same value as the KEY field
				Object ruleValue = ruleFields.get(keyEntryKey);
				if (!ruleValue.equals(keyEntryValue)) {
					isMatch = false;
					break;
				}
			}

			// if all KEY fields were matched in the RULE fields then this is
			// the item we need
			if (isMatch) {
				return t;
			}
		}

		return null;
	}
}
