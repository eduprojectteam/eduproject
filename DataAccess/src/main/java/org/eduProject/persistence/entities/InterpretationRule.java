package org.eduProject.persistence.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.eduProject.framework.entities.Rule;

@Entity
@IdClass(LongIdRuleKey.class)
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class InterpretationRule extends Rule implements Serializable {

	private static final long serialVersionUID = -3813800865889512074L;

	@XmlElement
	@Id
	private Long id;

	@XmlElement
	private List<VehicleAttribute> targetVehicle;

	@XmlElement
	private List<DefaultRule> rules;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<VehicleAttribute> getTargetVehicle() {
		return targetVehicle;
	}

	public void setTargetVehicle(List<VehicleAttribute> targetVehicle) {
		this.targetVehicle = targetVehicle;
	}

	public List<DefaultRule> getRules() {
		return rules;
	}

	public void setRules(List<DefaultRule> rules) {
		this.rules = rules;
	}
}
