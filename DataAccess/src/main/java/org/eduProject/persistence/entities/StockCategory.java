package org.eduProject.persistence.entities;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

@XmlType(name = "StockCategory")
@XmlEnum
public enum StockCategory {
	@XmlEnumValue("Used")
	Used("Used"), 
	@XmlEnumValue("New")
	New("New");

	private final String value;

	private StockCategory(String v) {
		this.value = v;
	}

	public static StockCategory fromValue(String v) {
		for (StockCategory c : StockCategory.values()) {
			if (c.value.equals(v)) {
				return c;
			}
		}
		throw new IllegalArgumentException(v);
	}
}
