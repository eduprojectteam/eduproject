package org.eduProject.persistence.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.eduProject.framework.entities.Rule;

@Entity
@IdClass(MarketRuleKey.class)
@XmlRootElement(name = "marketRule")
@XmlAccessorType(XmlAccessType.FIELD)
public class MarketRule extends Rule implements Serializable {

	private static final long serialVersionUID = 7008369288131240021L;

	@XmlElement(name = "countryNumber")
	@Id
	private String countryNumber;

	@XmlElement(name = "branch")
	@Id
	private Integer branch;

	@XmlElement(name = "stockCategory")
	@Id
	private StockCategory stockCategory;

	@XmlElement(name = "active")
	private Boolean active;

	@XmlElement(name = "rule")
	private String rule;

	public String getCountryNumber() {
		return countryNumber;
	}

	public void setCountryNumber(String countryNumber) {
		this.countryNumber = countryNumber;
	}

	public Integer getBranch() {
		return branch;
	}

	public void setBranch(Integer branch) {
		this.branch = branch;
	}

	public StockCategory getStockCategory() {
		return stockCategory;
	}

	public void setStockCategory(StockCategory stockCategory) {
		this.stockCategory = stockCategory;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public String getRule() {
		return rule;
	}

	public void setRule(String rule) {
		this.rule = rule;
	}

}
