package org.eduProject.persistence.entities;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.NONE)
public class DefaultRule {

	@XmlElement
	private Long id;

	@XmlElement
	private String vehicleAttribute;
	
	@XmlElement
	private List<String> vehicleAttributeValues;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getVehicleAttribute() {
		return vehicleAttribute;
	}
	public void setVehicleAttribute(String vehicleAttribute) {
		this.vehicleAttribute = vehicleAttribute;
	}
	public List<String> getVehicleAttributeValues() {
		return vehicleAttributeValues;
	}
	public void setVehicleAttributeValues(List<String> vehicleAttributeValues) {
		this.vehicleAttributeValues = vehicleAttributeValues;
	}
	
}
