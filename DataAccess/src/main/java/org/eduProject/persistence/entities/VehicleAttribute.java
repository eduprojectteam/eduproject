package org.eduProject.persistence.entities;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.NONE)
public class VehicleAttribute {
	
	@XmlElement
	private String vehicleAttribute;

	@XmlElement
	private String vehicleAttributeValue;
	
	public String getVehicleAttribute() {
		return vehicleAttribute;
	}
	public void setVehicleAttribute(String vehicleAttribute) {
		this.vehicleAttribute = vehicleAttribute;
	}
	public String getVehicleAttributeValue() {
		return vehicleAttributeValue;
	}
	public void setVehicleAttributeValue(String vehicleAttributeValue) {
		this.vehicleAttributeValue = vehicleAttributeValue;
	}
}
