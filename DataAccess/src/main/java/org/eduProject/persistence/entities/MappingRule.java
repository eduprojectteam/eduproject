package org.eduProject.persistence.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.eduProject.framework.entities.Rule;

@Entity
@IdClass(LongIdRuleKey.class)
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class MappingRule extends Rule implements Serializable {

	private static final long serialVersionUID = -6479144446736568042L;

	@XmlElement
	@Id
	private Long id;

	@XmlElement
	private String vehicleAttribute;

	@XmlElement
	private String sourceValue;

	@XmlElement
	private String targetValue;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getVehicleAttribute() {
		return vehicleAttribute;
	}

	public void setVehicleAttribute(String vehicleAttribute) {
		this.vehicleAttribute = vehicleAttribute;
	}

	public String getSourceValue() {
		return sourceValue;
	}

	public void setSourceValue(String sourceValue) {
		this.sourceValue = sourceValue;
	}

	public String getTargetValue() {
		return targetValue;
	}

	public void setTargetValue(String targetValue) {
		this.targetValue = targetValue;
	}

}
