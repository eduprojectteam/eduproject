package org.eduProject.persistence.entities;


public class LongIdRuleKey {

	private String id;

	public LongIdRuleKey() {
		super();
	}

	public LongIdRuleKey(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}
