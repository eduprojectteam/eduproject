package org.eduProject.persistence.entities;

import java.io.IOException;

import org.codehaus.jackson.map.ObjectMapper;

public class MarketRuleKey {

	private String countryNumber;
	private Integer branch;
	private StockCategory stockCategory;

	public MarketRuleKey() {
		super();
	}

	public MarketRuleKey(String countryNumber, Integer branch,
			StockCategory stockCategory) {
		this.countryNumber = countryNumber;
		this.branch = branch;
		this.stockCategory = stockCategory;
	}

	public String getCountryNumber() {
		return countryNumber;
	}

	public void setCountryNumber(String countryNumber) {
		this.countryNumber = countryNumber;
	}

	public Integer getBranch() {
		return branch;
	}

	public void setBranch(Integer branch) {
		this.branch = branch;
	}

	public StockCategory getStockCategory() {
		return stockCategory;
	}

	public void setStockCategory(StockCategory stockCategory) {
		this.stockCategory = stockCategory;
	}

	/** Deserializes an Object of class MyClass from its JSON representation */
	public static MarketRuleKey fromString(String jsonRepresentation) {
		ObjectMapper mapper = new ObjectMapper(); // Jackson's JSON marshaller
		MarketRuleKey o = null;
		try {
			o = mapper.readValue(jsonRepresentation, MarketRuleKey.class);
		} catch (IOException e) {

			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return o;
	}
}
