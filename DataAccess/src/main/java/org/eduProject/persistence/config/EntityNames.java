package org.eduProject.persistence.config;

public class EntityNames {

	public static final String MarketRule = "market";
	public static final String MappingRule = "mapping";
	public static final String InterpretationRule = "interpretation";
}
