/*package org.eduProject.persistence.mocks;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import javax.inject.Inject;

import org.eduProject.framework.TypeException;
import org.eduProject.framework.entities.Rule;
import org.eduProject.framework.entities.RuleKey;
import org.eduProject.framework.persistence.IDataAccess;
import org.eduProject.framework.persistence.IDataAccessFactory;
import org.eduProject.persistence.config.EntityNames;
import org.eduProject.persistence.entities.MappingRule;
import org.eduProject.persistence.entities.MarketRule;
import org.eduProject.persistence.entities.MarketRuleKey;
import org.eduProject.persistence.entities.StockCategory;
import org.jglue.cdiunit.AdditionalClasses;
import org.jglue.cdiunit.CdiRunner;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

*//**
 * Test methods from MockDataAccess class
 * 
 * @author ioanaf
 *
 *//*

@RunWith(CdiRunner.class)
@AdditionalClasses(MockDataAccessFactory.class)
public class MockDataAccessTest {

	@Inject
	@MockDA
	private IDataAccessFactory dataAccessFactory;

	private IDataAccess<Rule, RuleKey> da;

	private MarketRule postMarketRule;
	private MarketRule putMarketRule;
	private MarketRuleKey putrk;
	private MarketRuleKey getrk;
	private MarketRuleKey wrongKey;
	private MappingRule mappingRule;

	@Before
	public void setUp() {

		da = dataAccessFactory.get(EntityNames.MarketRule);
		System.out.println("@Before - setUp");
	}

	public void createMarketRuleForPost() {

		postMarketRule = new MarketRule();
		postMarketRule.setActive(false);
		postMarketRule.setBranch(4);
		postMarketRule.setCountryNumber("uk");
		postMarketRule.setRule("market-uk-used");
		postMarketRule.setStockCategory(StockCategory.New);
	}

	public void createMarketRuleForPut() {

		putMarketRule = new MarketRule();
		putMarketRule.setActive(false);
		putMarketRule.setBranch(1);
		putMarketRule.setCountryNumber("uk");
		putMarketRule.setRule("market-uk-used");
		putMarketRule.setStockCategory(StockCategory.Used);

	}

	public void createMappingRule() {

		mappingRule = new MappingRule();
		mappingRule.setId(100L);
		mappingRule.setSourceValue("source100");
		mappingRule.setTargetValue("target100");
		mappingRule.setVehicleAttribute("atr100");

	}

	public void createMarketRuleKeyForPut() {

		putrk = new MarketRuleKey();
		putrk.setBranch(1);
		putrk.setCountryNumber("uk");
		putrk.setStockCategory(StockCategory.Used);

	}

	public void createMarketRuleKeyForGet() {

		getrk = new MarketRuleKey();
		getrk.setBranch(2);
		getrk.setCountryNumber("es");
		getrk.setStockCategory(StockCategory.Used);

	}

	public void createWrongKey() {

		wrongKey = new MarketRuleKey();
		wrongKey.setBranch(100);
		wrongKey.setCountryNumber("ro");
		wrongKey.setStockCategory(StockCategory.Used);

	}

	*//**
	 * The given rule is valid
	 *//*
	@Test
	public void testPostValidInput() {

		createMarketRuleForPost();

		boolean validInput = da.post(postMarketRule);
		assertTrue(validInput);

		System.out.println("@Test - testPostValidInput");

	}

	*//**
	 * 
	 * The rule (object) has wrong type
	 * 
	 *//*

	@Test
	public void testPostNotMatchingRule() {

		createMappingRule();

		boolean validInput = da.post(mappingRule);
		assertFalse(validInput);

		System.out.println("@Test - testPostNotMatchingRule");

	}

	@Test
	public void testPutValidInput() {

		createMarketRuleForPut();
		createMarketRuleKeyForPut();

		boolean validInput = da.put(putrk, putMarketRule);
		assertTrue(validInput);

		System.out.println("@Test - testPutValidInput");

	}

	*//**
	 * 
	 * There is no object with the given ID
	 * 
	 *//*

	@Test
	public void testPutIdNotFound() {

		createMarketRuleForPut();
		createWrongKey();

		boolean idNotFound = da.put(wrongKey, putMarketRule);
		assertFalse(idNotFound);

		System.out.println("@Test - testPutIdNotFound");

	}

	*//**
	 * The rule (object) has wrong type
	 * 
	 *//*

	@Test
	public void testPutWrongObject() {

		createMappingRule();
		createMarketRuleKeyForPut();

		boolean wrongObject = da.put(putrk, mappingRule);
		assertFalse(wrongObject);

		System.out.println("@Test - testPutWrongObject");

	}

	@Test
	public void testDeleteValidInput() {

		MarketRuleKey deleterk = new MarketRuleKey();
		deleterk.setBranch(1);
		deleterk.setCountryNumber("uk");
		deleterk.setStockCategory(StockCategory.New);

		boolean validInput = da.delete(deleterk);
		assertTrue(validInput);

		System.out.println("@Test - testDeleteValidInput");

	}

	*//**
	 * 
	 * There is no object with the given ID
	 * 
	 *//*

	@Test
	public void testDeleteIdNotFound() {

		createWrongKey();

		boolean idNotFound = da.delete(wrongKey);
		assertFalse(idNotFound);

		System.out.println("@Test - testDeleteIdNotFound");

	}

	@Test
	public void testGetValidInput() {

		createMarketRuleKeyForGet();

		Object validInput = da.get(getrk);

		assertNotNull(validInput);

		System.out.println("@Test - testGetValidInput");

	}

	*//**
	 * There is no object with the given ID
	 *//*

	@Test
	public void testGetIdNotFound() {

		createWrongKey();

		Object idNotFound = da.get(wrongKey);

		assertNull(idNotFound);

		System.out.println("@Test - testGetIdNotFound");

	}

	@Test
	public void testGetAsRuleValidInput() {

		createMarketRuleForPost();
		Rule t = null;

		try {
			t = da.getAsRule(postMarketRule);
		} catch (TypeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		assertNotNull(t);

		System.out.println("@Test - testGetAsRuleValidInput");

	}

	*//**
	 * 
	 * Handle exceptions in jUnit with annotation
	 * 
	 * @throws TypeException
	 *//*

	@Test(expected = TypeException.class)
	public void testGetAsRuleThrowsExceptionWhenObjectNotValid()
			throws TypeException {

		Rule t = da.getAsRule("FakeObject");

		System.out
				.println("@Test - testGetAsRuleThrowsExceptionWhenObjectNotValid");

	}

	@Test
	public void testGetAsRuleKeyValidInput() {

		createMarketRuleKeyForPut();
		RuleKey k = null;

		try {
			k = da.getAsRuleKey(putrk);
		} catch (TypeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		assertNotNull(k);

		System.out.println("@Test - testGetAsRuleKeyValidInput");

	}

	*//**
	 * 
	 * Handle exceptions in jUnit First approach: try-catch idom
	 * 
	 *//*
	@Test
	public void testGetAsRuleKeyThrowsExceptionWhenObjectNotValid() {

		RuleKey k = null;

		try {
			k = da.getAsRuleKey("FakeObject");
			fail("Should throw an exception if the object is not valid");
		} catch (TypeException e) {
			assertNull(k);
		}

		System.out
				.println("@Test - testGetAsRuleKeyThrowsExceptionWhenObjectNotValid");

	}

	*//**
	 * 
	 * Handle exceptions in jUnit Second approach: With annotation
	 * 
	 * @throws TypeException
	 *//*
	@Test(expected = TypeException.class)
	public void testGetAsRuleKeyThrowsExceptionWhenObjectNotValid2()
			throws TypeException {

		RuleKey k = da.getAsRuleKey("Fake Object");

		System.out
				.println("@Test - testGetAsRuleKeyThrowsExceptionWhenObjectNotValid2");

	}

}
*/