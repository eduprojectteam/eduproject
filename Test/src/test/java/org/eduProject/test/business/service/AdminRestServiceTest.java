package org.eduProject.test.business.service;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.cxf.endpoint.Server;
import org.apache.cxf.jaxrs.JAXRSBindingFactory;
import org.apache.cxf.jaxrs.JAXRSServerFactoryBean;
import org.apache.cxf.jaxrs.client.WebClient;
import org.eduProject.business.service.AdminRestService;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class AdminRestServiceTest extends Assert {

	private final static String ENDPOINT_ADDRESS = "http://localhost:9080/Business/rest";
	private final static String WADL_ADDRESS = ENDPOINT_ADDRESS + "?_wadl";
	private static Server server;

	@BeforeClass
	public static void initialize() throws Exception {
		startServer();
	}

	private static void startServer() throws Exception {
		JAXRSServerFactoryBean sf = new JAXRSServerFactoryBean();
		sf.setResourceClasses(AdminRestService.class);
		sf.setBindingId(JAXRSBindingFactory.JAXRS_BINDING_ID);
		sf.setAddress(ENDPOINT_ADDRESS);
		Server myServer = sf.create();
	}

	@AfterClass
	public static void destroy() throws Exception {
		server.stop();
		server.destroy();
	}

	@Test
	public void testGetBookWithWebClient() {
		WebClient client = WebClient.create(ENDPOINT_ADDRESS);
		client.accept(MediaType.APPLICATION_JSON);
		// client.type(MediaType.APPLICATION_JSON);
		client.path("admin/mapping");
		client.query("id", 1);
		Response response = client.get(Response.class);
		assertEquals(200, response.getStatus());
	}

}
