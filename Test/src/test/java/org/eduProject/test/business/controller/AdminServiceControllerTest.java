package org.eduProject.test.business.controller;

import javax.inject.Inject;

import org.eduProject.business.controller.AdminServiceController;
import org.eduProject.framework.entities.RuleKey;
import org.eduProject.persistence.config.EntityNames;
import org.eduProject.persistence.entities.MarketRule;
import org.eduProject.persistence.entities.StockCategory;
import org.eduProject.persistence.mocks.MockDataAccessFactory;
import org.jglue.cdiunit.AdditionalClasses;
import org.jglue.cdiunit.CdiRunner;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

@RunWith(CdiRunner.class)
@AdditionalClasses(MockDataAccessFactory.class)
public class AdminServiceControllerTest {
	@Before
	public void setUp() {

		System.out.println("@Before - setUp");
	}

	@Inject
	private AdminServiceController adminServCtrl;

	private MarketRule postMarketRule;
	private MarketRule putMarketRule;
	private RuleKey putKey;
	private RuleKey getKey;
	private RuleKey invalidKey;

	public void createMarketRuleForPost() {

		postMarketRule = new MarketRule();
		postMarketRule.setActive(false);
		postMarketRule.setBranch(4);
		postMarketRule.setCountryNumber("uk");
		postMarketRule.setRule("market-uk-used");
		postMarketRule.setStockCategory(StockCategory.New);
	}

	public void createMarketRuleForPut() {

		putMarketRule = new MarketRule();
		putMarketRule.setActive(false);
		putMarketRule.setBranch(1);
		putMarketRule.setCountryNumber("uk");
		putMarketRule.setRule("market-uk-used");
		putMarketRule.setStockCategory(StockCategory.Used);

	}

	public void createMarketRuleKeyForPut() {

		putKey = new RuleKey();
		putKey.put("branch", "1");
		putKey.put("countryNumber", "uk");
		putKey.put("stockCategory", StockCategory.Used.toString());

	}

	public void createMarketRuleKeyForGet() {

		getKey = new RuleKey();
		getKey.put("branch", "2");
		getKey.put("countryNumber", "es");
		getKey.put("stockCategory", StockCategory.Used.toString());

	}

	public void createInvalidMarketRuleKey() {

		invalidKey = new RuleKey();
		invalidKey.put("branch", "111");
		invalidKey.put("countryNumber", "rooo");
		invalidKey.put("stockCategory", StockCategory.Used.toString());

	}

	@Test
	public void testPostValidInput() {

		createMarketRuleForPost();

		boolean resultValid = adminServCtrl.post(EntityNames.MarketRule,
				postMarketRule);
		assertTrue(resultValid);

		System.out.println("@Test - testPostValidInput");

	}

	/**
	 * The object doesn't match the rule
	 */
	@Test
	public void testPostRuleObjectNotMatching() {

		createMarketRuleForPost();

		boolean ruleObjectNotOk = adminServCtrl.post(EntityNames.MappingRule,
				postMarketRule);
		assertFalse(ruleObjectNotOk);

		System.out.println("@Test - testPostRuleObjectNotMatching");
	}

	/**
	 * The object doesn't have correct format
	 */
	@Test
	public void testPostObjectNotCorrect() {

		boolean objectNotOk = adminServCtrl.post(EntityNames.MarketRule,
				"Ioana");
		assertFalse(objectNotOk);

		System.out.println("@Test - testPostObjectNotCorrect");

	}

	/**
	 * the id exists
	 */
	@Test
	public void testPutValidInput() {

		createMarketRuleForPut();
		createMarketRuleKeyForPut();

		boolean validInput = adminServCtrl.put(EntityNames.MarketRule, putKey,
				putMarketRule);
		assertTrue(validInput);

		System.out.println("@Test - testPutValidInput");

	}

	/**
	 * There is no object with the given ID
	 */
	@Test
	public void testPutIdNotFound() {

		createInvalidMarketRuleKey();
		createMarketRuleForPut();

		boolean idNotFound = adminServCtrl.put(EntityNames.MarketRule,
				invalidKey, putMarketRule);
		assertFalse(idNotFound);

		System.out.println("@Test - testPutIdNotFound");

	}

	/**
	 * The object doesn't match the rule
	 */
	@Test
	public void testPutRuleObjectNotMatching() {

		createMarketRuleForPut();
		createMarketRuleKeyForPut();

		boolean ruleObjectNotOk = adminServCtrl.put(EntityNames.MappingRule,
				putKey, putMarketRule);
		assertFalse(ruleObjectNotOk);

		System.out.println("@Test - testPutRuleObjectNotMatching");

	}

	@Test
	public void testPutObjectNotCorrect() {

		createMarketRuleKeyForPut();

		boolean objectNotOk = adminServCtrl.put(EntityNames.MarketRule, putKey,
				"Test");
		assertFalse(objectNotOk);

		System.out.println("@Test - testPutObjectNotCorrect");

	}

	@Test
	public void testDeleteValidInput() {

		RuleKey deleterk = new RuleKey();
		deleterk.put("branch", "1");
		deleterk.put("countryNumber", "uk");
		deleterk.put("stockCategory", StockCategory.New.toString());

		boolean validInput = adminServCtrl.delete(EntityNames.MarketRule,
				deleterk);
		assertTrue(validInput);

		System.out.println("@Test - testDeleteValidInput");

	}

	/**
	 * There is no object with the given ID
	 */

	@Test
	public void testDeleteIdNotFound() {

		createInvalidMarketRuleKey();

		boolean idNotFound = adminServCtrl.delete(EntityNames.MarketRule,
				invalidKey);
		assertFalse(idNotFound);

		System.out.println("@Test - testDeleteIdNotFound");

	}

	/**
	 * There is no object with the given rule key
	 */
	@Test
	public void testDeleteRuleNotFound() {

		createMarketRuleKeyForPut();

		boolean ruleNotFound = adminServCtrl.delete("Test", putKey);
		assertFalse(ruleNotFound);

		System.out.println("@Test - testDeleteRuleNotFound");

	}

	@Test
	public void testGetValidInput() {

		createMarketRuleKeyForGet();

		Object validInput = adminServCtrl.get(EntityNames.MarketRule, getKey);

		assertNotNull(validInput);

		System.out.println("@Test - testGetValidInput");

	}

	/**
	 * There is no object with the given rule key
	 */
	@Test
	public void testGetIdNotFound() {

		RuleKey key = new RuleKey();
		key.put("countryNumber", "ro");
		key.put("branch", "6");
		key.put("stockCategory", StockCategory.Used.toString());

		Object validInput = adminServCtrl.get(EntityNames.MarketRule, key);

		assertNull(validInput);

		System.out.println("@Test - testGetIdNotFound");

	}

	/**
	 * The given rule doesn't exist
	 */
	@Test
	public void testGetRuleNotFound() {

		createMarketRuleKeyForGet();

		Object ruleNotFound = adminServCtrl.get("Ioana", getKey);

		assertNull(ruleNotFound);

		System.out.println("@Test - testGetRuleNotFound");

	}

}
