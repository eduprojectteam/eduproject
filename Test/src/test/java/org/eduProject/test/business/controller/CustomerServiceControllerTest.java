package org.eduProject.test.business.controller;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import javax.inject.Inject;

import org.eduProject.business.controller.CustomerServiceController;
import org.eduProject.framework.entities.RuleKey;
import org.eduProject.persistence.config.EntityNames;
import org.eduProject.persistence.entities.StockCategory;
import org.eduProject.persistence.mocks.MockDataAccessFactory;
import org.jglue.cdiunit.AdditionalClasses;
import org.jglue.cdiunit.CdiRunner;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(CdiRunner.class)
@AdditionalClasses(MockDataAccessFactory.class)
public class CustomerServiceControllerTest {

	@Inject
	private CustomerServiceController custServCtrl;
	private RuleKey validKey;

	@Before
	public void setUp() {
		System.out.println("@Before - setUp");
	}

	public void createValidRuleKey() {
		validKey = new RuleKey();
		validKey.put("countryNumber", "uk");
		validKey.put("branch", "1");
		validKey.put("stockCategory", StockCategory.Used.toString());

	}

	@Test
	public void testGetValidInput() {

		createValidRuleKey();

		Object validInput = custServCtrl.get(EntityNames.MarketRule, validKey);

		assertNotNull(validInput);

		System.out.println("@Test - testGetValidInput");

	}

	/**
	 * The given rule doesn not exist
	 */
	@Test
	public void testGetRuleNotFound() {
		
		createValidRuleKey();

		Object ruleNotFound = custServCtrl.get("Ioana", validKey);

		assertNull(ruleNotFound);

		System.out.println("@Test - testGetRuleNotFound");

	}
	
	/**
	 * There is no object with the given rule key
	 */
	@Test
	public void testGetIdNotFound() {
		
		
		RuleKey key = new RuleKey();
		key.put("countryNumber", "ro");
		key.put("branch", "6");
		key.put("stockCategory", StockCategory.Used.toString());

		Object validInput = custServCtrl.get(EntityNames.MarketRule,key);

		assertNull(validInput);

		System.out.println("@Test - testGetIdNotFound");

	}
}
