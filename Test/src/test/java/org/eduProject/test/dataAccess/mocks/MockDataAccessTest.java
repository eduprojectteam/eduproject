package org.eduProject.test.dataAccess.mocks;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import javax.inject.Inject;

import org.eduProject.framework.TypeException;
import org.eduProject.framework.entities.Rule;
import org.eduProject.framework.entities.RuleKey;
import org.eduProject.framework.persistence.IDataAccess;
import org.eduProject.framework.persistence.IDataAccessFactory;
import org.eduProject.persistence.config.EntityNames;
import org.eduProject.persistence.entities.MappingRule;
import org.eduProject.persistence.entities.MarketRule;
import org.eduProject.persistence.entities.StockCategory;
import org.eduProject.persistence.mocks.MockDA;
import org.eduProject.persistence.mocks.MockDataAccessFactory;
import org.jglue.cdiunit.AdditionalClasses;
import org.jglue.cdiunit.CdiRunner;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import sun.util.logging.resources.logging;

@RunWith(CdiRunner.class)
@AdditionalClasses(MockDataAccessFactory.class)
public class MockDataAccessTest {

	@Inject
	@MockDA
	private IDataAccessFactory dataAccessFactory;

	private IDataAccess<Rule> da;

	private MarketRule postMarketRule;
	private MarketRule putMarketRule;
	private RuleKey putKey;
	private RuleKey getKey;
	private RuleKey invalidKey;
	private MappingRule mappingRule;

	@Before
	public void setUp() {

		da = dataAccessFactory.get(EntityNames.MarketRule);
		System.out.println("@Before - setUp");
	}

	public void createMarketRuleForPost() {

		postMarketRule = new MarketRule();
		postMarketRule.setActive(false);
		postMarketRule.setBranch(4);
		postMarketRule.setCountryNumber("uk");
		postMarketRule.setRule("market-uk-used");
		postMarketRule.setStockCategory(StockCategory.New);
	}

	public void createMarketRuleForPut() {

		putMarketRule = new MarketRule();
		putMarketRule.setActive(false);
		putMarketRule.setBranch(1);
		putMarketRule.setCountryNumber("uk");
		putMarketRule.setRule("market-uk-used");
		putMarketRule.setStockCategory(StockCategory.Used);

	}

	public void createMarketRuleKeyForPut() {

		putKey = new RuleKey();
		putKey.put("branch", "1");
		putKey.put("countryNumber", "uk");
		putKey.put("stockCategory", StockCategory.Used.toString());

	}

	public void createMarketRuleKeyForGet() {

		getKey = new RuleKey();
		getKey.put("branch", "2");
		getKey.put("countryNumber", "es");
		getKey.put("stockCategory", StockCategory.Used.toString());

	}

	public void createInvalidMarketRuleKey() {

		invalidKey = new RuleKey();
		invalidKey.put("branch", "111");
		invalidKey.put("countryNumber", "rooo");
		invalidKey.put("stockCategory", StockCategory.Used.toString());

	}

	public void createMappingRule() {

		mappingRule = new MappingRule();
		mappingRule.setId(100L);
		mappingRule.setSourceValue("source100");
		mappingRule.setTargetValue("target100");
		mappingRule.setVehicleAttribute("atr100");

	}

	@Test
	public void testPostValidInput() {

		createMarketRuleForPost();

		boolean validInput = da.post(postMarketRule);
		assertTrue(validInput);

		System.out.println("@Test - testPostValidInput");

	}

	@Test
	public void testPostNotMatchingRule() {

		createMappingRule();

		boolean validInput = da.post(mappingRule);
		assertFalse(validInput);

		System.out.println("@Test - testPostNotMatchingRule");

	}

	@Test
	public void testPutValidInput() {

		createMarketRuleForPut();
		createMarketRuleKeyForPut();

		boolean validInput = da.put(putKey, putMarketRule);
		assertTrue(validInput);

		System.out.println("@Test - testPutValidInput");

	}

	/**
	 * There is no object with the given ID
	 */
	@Test
	public void testPutIdNotFound() {

		createMarketRuleForPut();
		createInvalidMarketRuleKey();

		boolean idNotFound = da.put(invalidKey, putMarketRule);
		assertFalse(idNotFound);

		System.out.println("@Test - testPutIdNotFound");

	}

	/**
	 * The rule (object) has wrong type
	 */

	@Test
	public void testPutWrongObject() {

		createMappingRule();
		createMarketRuleKeyForPut();

		boolean wrongObject = da.put(putKey, mappingRule);
		assertFalse(wrongObject);

		System.out.println("@Test - testPutWrongObject");

	}

	@Test
	public void testDeleteValidInput() {

		RuleKey deleterk = new RuleKey();
		deleterk.put("branch", "1");
		deleterk.put("countryNumber", "uk");
		deleterk.put("stockCategory", StockCategory.New.toString());

		boolean validInput = da.delete(deleterk);
		assertTrue(validInput);

		System.out.println("@Test - testDeleteValidInput");

	}

	/**
	 * There is no object with the given ID
	 */
	@Test
	public void testDeleteIdNotFound() {

		createInvalidMarketRuleKey();

		boolean idNotFound = da.delete(invalidKey);
		assertFalse(idNotFound);

		System.out.println("@Test - testDeleteIdNotFound");

	}

	@Test
	public void testGetValidInput() {

		createMarketRuleKeyForGet();

		Object validInput = da.get(getKey);

		assertNotNull(validInput);

		System.out.println("@Test - testGetValidInput");

	}

	/**
	 * There is no object with the given ID
	 */
	@Test
	public void testGetIdNotFound() {

		createInvalidMarketRuleKey();

		Object idNotFound = da.get(invalidKey);

		assertNull(idNotFound);

		System.out.println("@Test - testGetIdNotFound");

	}

	@Test
	public void testGetAsRuleValidInput() {

		createMarketRuleForPost();
		Rule t = null;

		try {
			t = da.getAsRule(postMarketRule);
		} catch (TypeException e) {
			e.printStackTrace();
		}

		assertNotNull(t);

		System.out.println("@Test - testGetAsRuleValidInput");

	}

	/**
	 * Handle exceptions in jUnit with annotation
	 * 
	 * @throws TypeException
	 */
	@Test(expected = TypeException.class)
	public void testGetAsRuleThrowsExceptionWhenObjectNotValid()
			throws TypeException {

		Rule t = da.getAsRule("FakeObject");

		System.out
				.println("@Test - testGetAsRuleThrowsExceptionWhenObjectNotValid");

	}

}
