<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<jsp:useBean id="employer" type="org.eduProject.business.BackingBean" class="org.eduProject.business.BackingBean" scope="request">
    <jsp:setProperty name="employer" property="strName"/>
    <jsp:setProperty name="employer" property="strEmpID"/>
</jsp:useBean>
 
<html>
<body>
Name: <jsp:getProperty name="employer" property="strName"/>
<br>
Employer Id: <jsp:getProperty name="employer" property="strEmpID"/>
</body></html>