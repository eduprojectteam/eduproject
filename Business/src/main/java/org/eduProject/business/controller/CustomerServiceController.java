package org.eduProject.business.controller;

import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.eduProject.framework.entities.Rule;
import org.eduProject.framework.entities.RuleKey;
import org.eduProject.framework.persistence.IDataAccess;
import org.eduProject.framework.persistence.IDataAccessFactory;
import org.eduProject.persistence.mocks.MockDA;

public class CustomerServiceController {

	@Inject
	@MockDA
	private IDataAccessFactory dataAccessFactory;

	final static Logger logger = Logger
			.getLogger(CustomerServiceController.class);

	public Object get(String rule, RuleKey key) {

		IDataAccess<Rule> da = dataAccessFactory.get(rule);

		if (da != null) {
			return da.get(key);
		}

		else {
			logger.error("The given rule doesn't exist, so MockDataAccessFactory returned null");
			return null;
		}
	}
}
