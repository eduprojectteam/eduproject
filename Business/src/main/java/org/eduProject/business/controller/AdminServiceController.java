package org.eduProject.business.controller;

import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.eduProject.framework.TypeException;
import org.eduProject.framework.entities.Rule;
import org.eduProject.framework.entities.RuleKey;
import org.eduProject.framework.persistence.IDataAccess;
import org.eduProject.persistence.mocks.MockDA;
import org.eduProject.persistence.mocks.MockDataAccessFactory;

public class AdminServiceController {

	@Inject
	@MockDA
	private MockDataAccessFactory dataAccessFactory;

	final static Logger logger = Logger.getLogger(AdminServiceController.class);

	public boolean put(String rule, RuleKey key, Object obj) {

		IDataAccess<Rule> da = dataAccessFactory.get(rule);
		Rule r = null;
		if (da == null) {

			logger.error("The given rule doesn't exist, so MockDataAccessFactory returned null");
			return false;
		}

		try {
			r = da.getAsRule(obj);
		} catch (TypeException e1) {

			logger.error(e1.getMessage());
			return false;

		}

		return da.put(key, r);

	}

	public boolean post(String rule, Object obj) {

		IDataAccess<Rule> da = dataAccessFactory.get(rule);
		Rule r = null;

		if (da == null) {

			logger.error("The given rule doesn't exist, so MockDataAccessFactory returned null");
			return false;
		}

		try {
			r = da.getAsRule(obj);
		} catch (TypeException e) {
			logger.error(e.getMessage());
			return false;
		}

		return da.post(r);

	}

	/**
	 * Deletes the Object with the given id
	 * 
	 * @param rule
	 *            The rule (domain class) the searched object represents
	 * @param key
	 *            The key of the searched object
	 * @return True if the object was found and deleted, false if not
	 */

	public boolean delete(String rule, RuleKey key) {

		IDataAccess<Rule> da = dataAccessFactory.get(rule);
		if (da != null) {
			return da.delete(key);
		} else {
			logger.error("The given rule doesn't exist, so MockDataAccessFactory returned null");
			return false;
		}

	}

	/**
	 * Gets the Object with the given id
	 * 
	 * @param rule
	 *            The rule (domain class) the searched object represents
	 * @param key
	 *            The key of the searched object
	 * @return The object with the given key, or null if not found
	 */
	public Object get(String rule, RuleKey key) {

		// get the data access instance IDataAccess<Rule, RuleKey> da =
		IDataAccess<Rule> da = dataAccessFactory.get(rule);

		if (da != null) {
			return da.get(key);
		}

		else {
			logger.error("The given rule doesn't exist, so MockDataAccessFactory returned null");
			return null;
		}
		
	}
}
