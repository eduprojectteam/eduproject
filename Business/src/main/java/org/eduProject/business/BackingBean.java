package org.eduProject.business;

import org.apache.log4j.Logger;
import org.eduProject.persistence.Employer;

public class BackingBean {

	final static Logger logger = Logger.getLogger(BackingBean.class);

	private String strName;
	private String strEmpID;
	private Employer em;

	public BackingBean() {
		logger.info("Hello this is an info message from IoanaFoidas");
		em = new Employer();
	}

	public String getStrName() {
		this.strName = em.getStrName();
		return strName;
	}

	public void setStrName(String strName) {
		this.strName = strName;
	}

	public String getStrEmpID() {
		this.strEmpID = em.getStrEmpID();
		return strEmpID;
	}

	public void setStrEmpID(String strEmpID) {
		this.strEmpID = strEmpID;
	}

}
