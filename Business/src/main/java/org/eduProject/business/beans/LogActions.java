package org.eduProject.business.beans;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

/**
 * Info to be logged when a request is made
 * 
 * @author ioanaf
 *
 */
@RequestScoped
public class LogActions {

	@Inject
	private RequestCounter requestCounter;

	private long start;
	final static Logger logger = Logger.getLogger(LogActions.class);

	private String url;
	private ServiceType serviceType;
	private String method;

	/**
	 * The current in time in millis - when the request was made
	 */
	public void startLog(HttpServletRequest req, ServiceType serviceType) {

		this.start = System.currentTimeMillis();
		this.url = req.getRequestURI();
		this.method = req.getMethod();
		this.serviceType = serviceType;
	}

	/**
	 * Logs info about the given request: method, url, duration
	 * 
	 * @param req
	 *            The request to be logged
	 * @param serviceType
	 *            Type of service: admin or customer
	 */

	public void stopLog() {

		long end = System.currentTimeMillis();
		long counterValue;

		counterValue = requestCounter.incrementAndGetCount(serviceType);

		logger.info("AdminRequest: " + counterValue + " " + method + " " + url
				+ " " + (end - start) + "ms");

	}
}
