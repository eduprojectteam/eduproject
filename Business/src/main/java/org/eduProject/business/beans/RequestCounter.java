package org.eduProject.business.beans;

import java.util.concurrent.atomic.AtomicLong;

import javax.enterprise.context.ApplicationScoped;

/**
 * Defines a counter for the requests One counter for each service
 * 
 * @author ioanaf
 *
 */
@ApplicationScoped
public class RequestCounter {

	private AtomicLong adminCount = new AtomicLong(0L);
	private AtomicLong customerCount = new AtomicLong(0L);

	/**
	 * increment count when count reaches long MAX_VALUE, it is reset to 0
	 */
	public long incrementAndGetCount(ServiceType serviceType) {

		if (ServiceType.Admin == serviceType) {
			return adminCount.incrementAndGet() % (Long.MAX_VALUE + 1);
		} else {
			return customerCount.incrementAndGet() % (Long.MAX_VALUE + 1);
		}

	}
}
