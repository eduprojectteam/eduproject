package org.eduProject.business.service;

import org.eduProject.framework.entities.Rule;
import org.eduProject.persistence.config.EntityNames;
import org.eduProject.persistence.entities.InterpretationRule;
import org.eduProject.persistence.entities.MappingRule;
import org.eduProject.persistence.entities.MarketRule;

public class RuleProviderFactory {

	public <T extends Rule> RuleProvider<T> get(String rule) {

		if (rule.equals(EntityNames.MarketRule)) {
			RuleProvider<MarketRule> marketRule = new RuleProvider<MarketRule>(
					MarketRule.class);
			return (RuleProvider<T>) marketRule;
		}

		if (rule.equals(EntityNames.MappingRule)) {
			RuleProvider<MappingRule> mappingRule = new RuleProvider<MappingRule>(
					MappingRule.class);
			return (RuleProvider<T>) mappingRule;
		}

		if (rule.equals(EntityNames.InterpretationRule)) {
			RuleProvider<InterpretationRule> interpretationRule = new RuleProvider<InterpretationRule>(
					InterpretationRule.class);
			return (RuleProvider<T>) interpretationRule;
		}

		return null;
	}
}
