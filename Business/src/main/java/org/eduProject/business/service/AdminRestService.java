package org.eduProject.business.service;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.UriInfo;

import org.apache.log4j.Logger;
import org.eduProject.business.beans.LogActions;
import org.eduProject.business.beans.ServiceType;
import org.eduProject.business.controller.AdminServiceController;
import org.eduProject.framework.entities.Rule;
import org.eduProject.framework.entities.RuleKey;

@Path(value = "/admin")
public class AdminRestService {

	final static Logger logger = Logger.getLogger(AdminRestService.class);

	@Inject
	private AdminServiceController adminController;

	@Inject
	private LogActions logActions;

	@Inject
	private RuleDecoder ruleDecoder;

	@Context
	private HttpServletRequest context;

	@Context
	UriInfo uriInfo;

	@PUT
	@Path(value = "{rule}")
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public Response putRule(@PathParam("rule") String rule, String obj) {

		logActions.startLog(context, ServiceType.Admin);

		String mediaType = context.getContentType();

		// RuleDecoder ruleDecoder = new RuleDecoder();
		Rule decodedRule = ruleDecoder.decode(mediaType, rule, obj);

		ResponseBuilder response = Response.ok();

		RuleKey key = new RuleKeyParser().parse(uriInfo);

		if (key.size() == 0) {
			response = Response.serverError().entity(
					"Resource Id cannot be blank");
		}

		try {
			boolean statusOk = adminController.put(rule, key, decodedRule);
			if (statusOk) {
				response = Response.ok(decodedRule);
			} else {
				response = Response.status(Response.Status.NOT_FOUND).entity(
						"Entity not found for Id: " + key);
			}

		} catch (Exception e) {
			logger.error(e.getMessage());
			response = Response.status(Response.Status.INTERNAL_SERVER_ERROR);
		}

		System.out.println("put");

		logActions.stopLog();

		return response.build();
	}

	@POST
	@Path(value = "{rule}")
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public Response postRule(@PathParam("rule") String rule, String obj) {

		logActions.startLog(context, ServiceType.Admin);

		String mediaType = context.getContentType();

		// RuleDecoder ruleDecoder = new RuleDecoder();
		Rule decodedRule = ruleDecoder.decode(mediaType, rule, obj);

		ResponseBuilder response = Response.ok();

		try {
			boolean statusOk = adminController.post(rule, decodedRule);
			if (statusOk) {
				response = Response.ok(decodedRule);
			} else {
				response = Response
						.status(Response.Status.INTERNAL_SERVER_ERROR);
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			response = Response.status(Response.Status.INTERNAL_SERVER_ERROR);
		}
		System.out.println("post");

		logActions.stopLog();

		return response.build();
	}

	@DELETE
	@Path(value = "/{rule}")
	public Response deleteRule(@PathParam("rule") String rule) {

		logActions.startLog(context, ServiceType.Admin);
		ResponseBuilder response = Response.ok();

		RuleKey key = new RuleKeyParser().parse(uriInfo);

		if (key.size() == 0) {
			response = Response.serverError().entity(
					"Resource Id cannot be blank");
		}

		try {
			boolean statusOk = adminController.delete(rule, key);
			if (statusOk) {
				response = Response.ok();
			} else {
				response = Response.status(Response.Status.NOT_FOUND).entity(
						"Entity not found for Id: " + key);
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			response = Response.status(Response.Status.INTERNAL_SERVER_ERROR);
		}
		System.out.println("delete");

		logActions.stopLog();

		return response.build();
	}

	@GET
	@Path("/{rule}")
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public Response getRule(@PathParam("rule") String rule) {

		logActions.startLog(context, ServiceType.Admin);
		ResponseBuilder response = Response.ok();

		RuleKey key = new RuleKeyParser().parse(uriInfo);
		if (key.size() == 0) {
			response = Response.serverError().entity(
					"Resource Id cannot be blank");
		}

		try {
			Object result = adminController.get(rule, key);

			if (result != null) {
				response = Response.ok().entity(result);
			} else {
				response = Response.status(Response.Status.NOT_FOUND).entity(
						"Entity not found for Id: " + key);
			}

		} catch (Exception e) {
			logger.error(e.getMessage());
			response = Response.status(Response.Status.INTERNAL_SERVER_ERROR);
		}
		System.out.println("get");

		logActions.stopLog();

		return response.build();
	}

}
