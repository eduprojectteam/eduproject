package org.eduProject.business.service;

import java.util.List;
import java.util.Map.Entry;

import javax.ws.rs.core.UriInfo;

import org.eduProject.framework.entities.RuleKey;

public class RuleKeyParser {

	public RuleKey parse(UriInfo uriInfo) {

		RuleKey key = new RuleKey();
		for (Entry<String, List<String>> entry : uriInfo.getQueryParameters()
				.entrySet()) {
			key.put(entry.getKey(), entry.getValue().get(0));
		}

		return key;
	}
}
