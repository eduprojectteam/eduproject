package org.eduProject.business.service;

import java.io.IOException;
import java.io.StringReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.eduProject.framework.entities.Rule;

public class RuleProvider<T extends Rule> {

	private final Class<T> type;
	final static Logger logger = Logger.getLogger(RuleProvider.class);

	public RuleProvider(Class<T> type) {
		this.type = type;
	}

	public T getRule(String format, String object) {
		ObjectMapper om = new ObjectMapper();
		T t;
		if (format.equals("application/json")) {
			try {
				t = om.readValue(object, type);
				return t;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				logger.error(e.getMessage());	
			}
		}
		else {
			try {
				JAXBContext jaxbContext = JAXBContext.newInstance(type);
				Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
				
				StringReader reader = new StringReader(object);
				T rule = (T) unmarshaller.unmarshal(reader);
				return rule;
			} catch (JAXBException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				logger.error(e.getMessage());
			}
		}
		return null;
	}
}
