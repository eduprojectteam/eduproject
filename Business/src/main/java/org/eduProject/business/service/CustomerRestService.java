package org.eduProject.business.service;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.CacheControl;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.UriInfo;

import org.apache.log4j.Logger;
import org.eduProject.business.beans.LogActions;
import org.eduProject.business.beans.ServiceType;
import org.eduProject.business.controller.CustomerServiceController;
import org.eduProject.framework.entities.RuleKey;

@Path(value = "/customer")
public class CustomerRestService {

	@Inject
	CustomerServiceController customerController;

	@Inject
	private LogActions logAction;

	final static Logger logger = Logger.getLogger(CustomerRestService.class);

	@Context
	private HttpServletRequest context;

	@Context
	UriInfo uriInfo;

	private static final int CACHE_LIFETIME = 86400;

	@GET
	@Path(value = "{rule}")
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public Response getRule(@PathParam("rule") String rule) {

		logAction.startLog(context, ServiceType.Customer);
		ResponseBuilder response = Response.ok();

		// add cache control
		CacheControl cc = new CacheControl();
		cc.setMaxAge(CACHE_LIFETIME);
		cc.setPrivate(true);

		response.cacheControl(cc);

		RuleKey key = new RuleKeyParser().parse(uriInfo);

		if (key.size() == 0) {
			response = Response.serverError().entity(
					"Resource Id cannot be blank");
			return response.build();
		}

		try {
			Object result = customerController.get(rule, key);
			
			if (result != null) {
				response = Response.ok().entity(result);
			} else {
				response = Response.status(Response.Status.NOT_FOUND).entity(
						"Entity not found for Id: " + key);
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			response = Response.status(Response.Status.INTERNAL_SERVER_ERROR);
		}

		System.out.println("customer get");

		logAction.stopLog();

		return response.build();
	}
}
