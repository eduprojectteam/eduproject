package org.eduProject.business.service;

import javax.inject.Inject;

import org.eduProject.framework.entities.Rule;

public class RuleDecoder {
	
	@Inject
	private RuleProviderFactory ruleProviderFactory; 
	
	public Rule decode(String format, String rule, String object) {
		RuleProvider<Rule> ruleProvider = ruleProviderFactory.get(rule);
		Rule providedRule = ruleProvider.getRule(format, object);
		return providedRule;
	}
}
