package org.eduProject.framework.persistence;

import org.eduProject.framework.entities.Rule;

public interface IDataAccessFactory {

	/**
	 * Creates an instance of a IDataAccess<T,K> <T>: the type of domain class
	 * <K>: the id type of the domain class
	 * 
	 * @param rule
	 *            The name of the domain class
	 * @return A new instance of IDataAccess<T,K>
	 */
	public <T extends Rule> IDataAccess<T> get(String rule);
}
