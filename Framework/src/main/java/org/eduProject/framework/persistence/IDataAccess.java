package org.eduProject.framework.persistence;

import org.eduProject.framework.TypeException;
import org.eduProject.framework.entities.Rule;
import org.eduProject.framework.entities.RuleKey;

public interface IDataAccess<T extends Rule> {

	public boolean post(T object);

	public boolean put(RuleKey id, T object);

	public boolean delete(RuleKey id);

	public T get(RuleKey id);

	/**
	 * Converts the given object to the <T extends Rule> type
	 * 
	 * @param type
	 *            The type to convert to
	 * @return The type object as <T extends Rule>
	 * @throws ClassCastException
	 *             Exception is thrown when the given object is not of a type <T
	 *             extends Rule>
	 */
	public T getAsRule(Object type) throws TypeException;
}
