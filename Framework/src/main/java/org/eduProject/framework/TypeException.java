package org.eduProject.framework;

public class TypeException extends Exception {

	private static final long serialVersionUID = 958289669204536416L;

	public TypeException() {
	}

	public String getMessage() {
		return "Conversion Error!";
	}

}
